import { createRouter, createWebHistory } from "vue-router";
import LoginUser from "../views/showViewLogin.vue";
import RegisterUser from "../views/showViewRegister.vue";
import Home from "../views/Home.vue";
import store from '../store/store.js'

const routes = [
  {
    path: "/register",
    name: "register",
    component: RegisterUser,
  },
  {
    path: "/Home",
    name: "home",
    component: Home,
    meta: {requiresAuth: true}
  },
  {
    path: "/login",
    name: "login",
    component: LoginUser,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach ((to,from,next)=>{ //manipulacion de la ruta para poder acceder solo con autenticacion
  
if (to.name !== 'login' && to.name !== 'register' && !store.state.stateLogin) {
  next({
    path:'/login',
    replace: true
  })
}else{
    next()
}
})
export default router;

// router.beforeEach((to,from,next)=>{
//   if (to.name !== 'login' && !store.mutations.changeState) {
//     next({
//       path:'/login',
//       replace: true
//     })
//   }else{
//     next({path:'/login',
//     replace: true
//   })
//   }
//   })