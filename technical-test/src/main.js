import "bootstrap/dist/css/bootstrap.min.css";
import { createApp } from "vue";
import { useWindowSize } from 'vue-window-size'
import App from "./App.vue";
import router from "./router";
import store from '../src/store/store.js';
createApp(App).use(router).use(store).use(useWindowSize).mount("#app");
import "bootstrap/dist/js/bootstrap.min";
