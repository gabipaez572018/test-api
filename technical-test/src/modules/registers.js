// import HelloWorld from "@/components/HelloWorld.vue";
export default {
  name: "RegisterUser",
  data() {
    return {
      userDataNatural: {
        name: "",
        lastName: "",
        email: "",
        password: "",
        phoneNumber: "",
        identificationNumber: "",
        confirmPassword: "",
      },
      userDataLegal: {
        name: "",
        NIT: "",
        phoneNumber: "",
        email: "",
        password: "",
        confirmPassword: "",
       },
      typePerson: "Natural",
      showWidth:window.screen.width,
    };
  },
  methods: {
    registerN () {
        this.$store.commit('getUserInfoN',{
          nameN:this.userDataNatural
        })
        this.$router.push({ path: 'Login' })
    }, 	
    registerL() {
    this.$store.commit('getUserInfoL',{
      namel:this.userDataLegal,
   })
   this.$router.push({ path: 'Login' })
},
    showForm() {
      return this.typePerson === "Natural";
    },
    showW() {
      return this.showWidth <= 600;
    },
  },
};

// components: {
//   HelloWorld,
// },
