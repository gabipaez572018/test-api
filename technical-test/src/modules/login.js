import { mapActions } from "vuex";
export default {
    name: "LoginUserM",
    data() {
        return {
            passwordL: "",
            nameU: "",

        };
    },
    methods: {
        loginUserLocal() {
            let dataStore = window.localStorage.getItem("user");
            dataStore = JSON.parse(dataStore);
            if (dataStore.nameN) {
                if (
                    dataStore.nameN.password === this.passwordL &&
                    dataStore.nameN.name === this.nameU
                ) {
                    this.contextLoginState()
                    this.$router.push({ path: 'Home' })
                }
            } else {
                if (
                    dataStore.namel.password === this.passwordL &&
                    dataStore.namel.name === this.nameU
                ) {
                   this.contextLoginState();
                   this.$router.push({ path: 'Home' })
                }
            }
        },
        loginUser() {
             this.loginUserLocal();
        },
    ...mapActions([
        'contextLoginState'
    ])
    },
};