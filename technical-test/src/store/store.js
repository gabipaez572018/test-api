import { createStore } from "vuex";

const store = createStore({
  state: {
    userDataNatural: {
      name: "",
      lastName: "",
      email: "",
      password: "",
      phoneNumber: "",
      identificationNumber: "",
      confirmPassword: "",
    },
    userDataLegal: {
      name: "",
      NIT: "",
      phoneNumber: "",
      email: "",
      password: "",
      confirmPassword: "",
    },
    stateLogin: false
  },
  mutations: {
    getUserInfoN(state, userN) {
      state.userDataNatural = userN;
      localStorage.setItem("user", JSON.stringify(state.userDataNatural));
    },
    getUserInfoL(state, userL){
    state.userDataLegal = userL;
      localStorage.setItem("user", JSON.stringify(state.userDataLegal));
    },
    changeState(state){
        state.stateLogin = true;
        }
  },
  getters: {
    
  },
  actions: {
      contextLoginState(context){
          context.commit('changeState');
        //   setTimeout(function(){
        //  console.log(context.commit('changeState'))
        // }, 3000)
      }
  }
});

export default store